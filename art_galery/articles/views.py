from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Article, Category
from django.urls import reverse_lazy
from django.views import generic
from authors.models import Author
from .forms import ArticleForm

# Articles
class ArticleList(LoginRequiredMixin, generic.ListView):
  model = Article

class ArticleDetail(LoginRequiredMixin, generic.DetailView):
  model = Article

class ArticleCreate(LoginRequiredMixin, generic.CreateView):
  model = Article
  form_class = ArticleForm
  success_url = reverse_lazy('articles:article_list')

  def form_valid(self, form):
    author = Author.objects.get(user=self.request.user)
    form.instance.author = author
    return super().form_valid(form)

class ArticleUpdate(LoginRequiredMixin, generic.UpdateView):
  model = Article
  form_class = ArticleForm
  success_url = reverse_lazy('articles:article_list')

class ArticleDelete(LoginRequiredMixin, generic.DeleteView):
  model = Article
  success_url = reverse_lazy('articles:article_list')

# Categories
class CategoryList(LoginRequiredMixin, generic.ListView):
  model = Category

class CategoryDetail(LoginRequiredMixin, generic.DetailView):
  model = Category