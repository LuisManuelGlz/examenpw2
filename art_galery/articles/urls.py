from django.urls import path
from . import views

app_name = 'articles'
urlpatterns = [
  path('', views.ArticleList.as_view(), name='article_list'),
  path('<int:pk>/', views.ArticleDetail.as_view(), name='article_detail'),
  path('create/', views.ArticleCreate.as_view(), name='article_create'),
  path('update/<int:pk>/', views.ArticleUpdate.as_view(), name='article_update'),
  path('delete/<int:pk>/', views.ArticleDelete.as_view(), name='article_delete'),
  path('categories/', views.CategoryList.as_view(), name='category_list'),
  path('categories/<int:pk>/', views.CategoryDetail.as_view(), name='category_detail'),
]