from django.db import models
from authors.models import Author
class Category(models.Model):
  name = models.CharField(max_length=100)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  class Meta:
    verbose_name_plural = 'categories'
    ordering = ['name']
  
  def __str__(self):
    return self.name

class Article(models.Model):
  title = models.CharField(max_length=200)
  content = models.TextField()
  author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='get_articles')
  categories = models.ManyToManyField(Category, related_name='get_articles')
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  class Meta:
    ordering = ['-created_at']
  
  def __str__(self):
    return self.title
