from django import forms
from .models import Article, Category

class ArticleForm(forms.ModelForm):
  title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'autofocus': True}))
  content = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '5'}))

  class Meta:
    model = Article
    fields = ['title', 'content', 'categories',]
