from django.views import generic
from .models import Author
from articles.models import Article
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404

class AuthorList(LoginRequiredMixin, generic.ListView):
  model = Author

class AuthorDetail(LoginRequiredMixin, generic.DetailView):
  model = Author
