from django.urls import path
from . import views

app_name = 'authors'
urlpatterns = [
  path('', views.AuthorList.as_view(), name='author_list'),
  path('<int:pk>/', views.AuthorDetail.as_view(), name='author_detail'),
]