from django.shortcuts import render
from django.views import generic
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy

class Home(generic.TemplateView):
  template_name = 'home/home.html'

class Signup(generic.CreateView):
  template_name = 'home/signup.html'
  form_class = UserCreationForm
  success_url = reverse_lazy('home:login')