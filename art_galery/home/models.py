from django.db import models
from django.contrib.auth.models import User
from authors.models import Author
from django.dispatch import receiver
from django.db.models.signals import post_save

@receiver(post_save, sender=User)
def ensure_author_exists(sender, instance, **kwargs):
  if kwargs.get('created', False):
    Author.objects.get_or_create(user=instance)

