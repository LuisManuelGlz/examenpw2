from django.db import models

class Cundina(models.Model):
  goal = models.DecimalField(max_digits=7, decimal_places=2)
  current_amount = models.DecimalField(max_digits=7, decimal_places=2, default=0)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def __str__(self):
    return f"Cundina #{self.id}"

  class Meta:
    verbose_name = "Cundina"
    verbose_name_plural = "Cundinas"