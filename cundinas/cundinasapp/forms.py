from django import forms
from .models import Cundina

class CundinaForm(forms.ModelForm):
  # goal = forms.DecimalField(max_digits=7, decimal_places=2)

  class Meta:
    model = Cundina
    fields = ['goal',]