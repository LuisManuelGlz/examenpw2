from django.urls import path
from . import views

app_name = 'cundinas'
urlpatterns = [
  path('', views.CundinaList.as_view(), name='cundina_list'),
  path('create/', views.CundinaCreate.as_view(), name='cundina_create'),
]