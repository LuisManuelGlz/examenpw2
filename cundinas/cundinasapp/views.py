from django.shortcuts import render
from .models import Cundina
from django.views import generic
from django.urls import reverse_lazy, reverse
from .forms import CundinaForm
from django.contrib.auth.mixins import LoginRequiredMixin

class CundinaList(LoginRequiredMixin, generic.ListView):
  model = Cundina
  template_name = 'cundinasapp/cundina_list.html'

# class CundinaDetail(generic.DetailView):
#   model = Cundina
#   template_name = 'cundinasapp/cundina_detail.html'

class CundinaCreate(LoginRequiredMixin, generic.CreateView):
  model = Cundina
  form_class = CundinaForm
  template_name = 'cundinasapp/cundina_form.html'
  success_url = reverse_lazy('cundinas:cundina_list')
