from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

class SavingManager(models.Model):
  user = models.OneToOneField(User, on_delete=models.CASCADE)

  def __str__(self):
      return self.user.username

@receiver(post_save, sender=User)
def ensure_savingmanager_exists(sender, instance, **kwargs):
  if kwargs.get('created', False):
    SavingManager.objects.get_or_create(user=instance)
