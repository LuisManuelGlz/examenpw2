from django.db import models
from cundinasapp.models import Cundina

class Contributor(models.Model):
  name = models.CharField(max_length=250)
  amount = models.DecimalField(max_digits=7, decimal_places=2)
  total_amount = models.DecimalField(max_digits=7, decimal_places=2, default=0)
  cundina = models.ForeignKey(Cundina, on_delete=models.CASCADE, related_name='contributors')
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def __str__(self):
      return self.name

  class Meta:
    ordering = ['-created_at',]
