from django import forms
from .models import Contributor

class ContributorForm(forms.ModelForm):
  amount = forms.DecimalField(min_value=0)

  class Meta:
    model = Contributor
    fields = ['name', 'amount','cundina',]
