from django.shortcuts import get_object_or_404
from django.views import generic
from django.urls import reverse_lazy, reverse
from .models import Contributor
from cundinasapp.models import Cundina
from .forms import ContributorForm
from django.contrib.auth.mixins import LoginRequiredMixin

class ContributorList(LoginRequiredMixin, generic.ListView):
  model = Contributor
  template_name = 'contributors/contributor_list.html'

  def get_queryset(self):
    cundina = get_object_or_404(Cundina, id=self.kwargs['pk'])
    return Contributor.objects.filter(cundina=cundina).order_by('-created_at')

class ContributorCreate(LoginRequiredMixin, generic.CreateView):
  model = Contributor
  form_class = ContributorForm
  template_name = 'contributors/contributor_form.html'

  def get_success_url(self):
    return self.request.path

class ContributorUpdateAmount(LoginRequiredMixin, generic.RedirectView):
  def get_redirect_url(self, *args, **kwargs):
    contributor = get_object_or_404(Contributor, pk=kwargs['pk'])
    contributor.total_amount += contributor.amount
    contributor.save()

    cundina = get_object_or_404(Cundina, pk=contributor.cundina.id)
    cundina.current_amount += contributor.amount
    cundina.save()

    return reverse('contributors:contributor_list', args=(kwargs['cunpk'],))
    # return super().get_redirect_url(*args, **kwargs)
