from django.urls import path, reverse_lazy
from . import views

app_name = 'contributors'
urlpatterns = [
  path('<int:pk>/', views.ContributorList.as_view(), name='contributor_list'),
  path('create/', views.ContributorCreate.as_view(), name='contributor_create'),
  path('update_amount/<int:cunpk>/<int:pk>/', views.ContributorUpdateAmount.as_view(), name='contributor_update_amount'),
]
