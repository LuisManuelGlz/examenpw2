## Antes de empezar

Comandos para PostgreSQL

```bash
create database art_galery_db;
create user art_galery_user with password '123';
grant all privileges on database art_galery_db to art_galery_user;

create database cundinas_db;
create user cundinas_user with password '123';
grant all privileges on database cundinas_db to cundinas_user;
```

---

Para utilizar los proyectos es necesario ejecutar lo siguiente.

```bash
# Creamos el entorno virtual
virtualenv -p python3 venv

# Activamos el entorno virtual
source venv/bin/activate

# Instalamos los requerimientos
pip install -r requirements.txt

# Realizar las migraciones para cada proyecto
python manage.py migrate
```
